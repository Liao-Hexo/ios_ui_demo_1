//
//  ViewController.m
//  Addition calculator-加法计算器
//
//  Created by 廖家龙 on 2020/4/18.
//  Copyright © 2020 liuyuecao. All rights reserved.
//

#import "ViewController.h"

//类扩展
@interface ViewController ()

//IBAction生成方法，跟void一样，只是它可以建立连线
- (IBAction)calculate;//计算方法的声明

//IBOutlet生成属性
@property (weak, nonatomic) IBOutlet UITextField *text1; //第一个文本框

@property (weak, nonatomic) IBOutlet UITextField *text2; //第二个文本框

@property (weak, nonatomic) IBOutlet UILabel *result; //最后计算显示的结果

@end


@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

//计算方法的实现
- (IBAction)calculate {
    
    //1.获取用户的输入(因为用户输入的是整型数字，text是NSString类型的，所以后面要用intValue)
// 1）第一种方法：
//    NSString *num1=self.text1.text;
//    NSString *num2=self.text2.text;
//    int n1=[num1 intValue];
//    int n2=num2.intValue;
// 2）第二种方法：
    int n1=self.text1.text.intValue;
    int n2=self.text2.text.intValue;
    
    //2.计算和
    int result=n1+n2;
    
    //3.把结果显示到结果result【把数字转化为字符串：stringWithFormat】
    self.result.text=[NSString stringWithFormat:@"%d",result];
    
    //小技巧：把键盘叫回去的两种方法
    
    //1.谁叫出的键盘，谁就是第一响应者，让第一响应者辞职，就可以把键盘叫回去
    //[self.text1 resignFirstResponder];
    //[self.text2 resignFirstResponder];
    
    //2.让控制器所管理的view停止编辑，也可以把键盘叫回去
      [self.view endEditing:YES];
}
@end
